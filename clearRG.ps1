﻿#Clear ResourceGroup:
$rg = 'jfuchsResourceGroup'
$rg

$sqlsrvs = Get-AzureRmResource | Where-Object {($_.ResourceGroupName -eq $rg) -and ($_.ResourceType -eq 'Microsoft.Sql/servers')}
$sqlsrvs.Count
$sqlsrvs | %{Remove-AzureRmSqlServer -ResourceGroupName $rg -ServerName $_.Name -Force; $_.Name + ' is removed.'}

$stans = Get-AzureRmResource | Where-Object {($_.ResourceGroupName -eq $rg) -and ($_.ResourceType -eq 'Microsoft.Storage/storageAccounts')}
$stans | %{Remove-AzureRmStorageAccount -ResourceGroupName $rg -Name $_.Name -Force; $_.Name + ' is removed.'}

$vnets = Get-AzureRmResource | Where-Object {($_.ResourceGroupName -eq $rg) -and ($_.ResourceType -eq 'Microsoft.Network/virtualNetworks')}
$vnets | %{Remove-AzureRmVirtualNetwork -ResourceGroupName $rg -Name $_.Name -Force; $_.Name + ' is removed.'}

$kvs = Get-AzureRmResource | Where-Object {($_.ResourceGroupName -eq $rg) -and ($_.ResourceType -eq 'Microsoft.KeyVault/vaults')}
$kvs | %{Remove-AzureRmKeyVault -ResourceGroupName $rg -VaultName $_.Name -Force; $_.Name + ' is removed.'}