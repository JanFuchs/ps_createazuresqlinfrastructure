﻿#Create an Azure Sql Database
[CmdletBinding()]
param (
    
     [Parameter(Mandatory=$True)] [string]$dbn
    ,[string]$useSample = 'False'
    ,[string]$ObjectiveName = 'S0'
    ,[string]$Collation = 'SQL_Latin1_CP1_CI_AS'
)

$lc = $global:lc
$SQLsn = $global:SQLsn
$rg = $global:rg
$stan = $global:stan

if ($useSample -eq 'False')
{
    New-AzureRmSqlDatabase `
        -ResourceGroupName $rg `
        -ServerName $SQLsn `
        -DatabaseName $dbn `
        -RequestedServiceObjectiveName $ObjectiveName `
        -CollationName $Collation
} else
{
    New-AzureRmSqlDatabase `
        -ResourceGroupName $rg `
        -ServerName $SQLsn `
        -DatabaseName $dbn `
        -SampleName AdventureWorksLT `
        -RequestedServiceObjectiveName $ObjectiveName `
        -CollationName $Collation
}

$time = (Get-Date).ToString()
'Sql Database ' + $dbn + ': Sql Database created (' + $time + ').'

Set-AzureRmSqlDatabaseAuditing -State Enabled -ResourceGroupName $rg -ServerName $SQLsn -StorageAccountName $stan -DatabaseName $dbn

$notemail = 'jfuchs.extern@airplus.com'
Set-AzureRmSqlDatabaseThreatDetectionPolicy -ResourceGroupName $rg -ServerName $SQLsn -DatabaseName $dbn -StorageAccountName $stan -NotificationRecipientsEmails $notemail -EmailAdmins $false

'Vulnerability Assessement has to configure manual.'

#FailoverGroup:
$fogn = (Get-AzureRmSqlDatabaseFailoverGroup -ResourceGroupName $rg -ServerName $SQLsn).FailoverGroupName

$time = (Get-Date).ToString()
'FailoverGroup ' + $fogn + ': Getting name of failover group (' + $time + ').'

$db = Get-AzureRmSqlDatabase -ResourceGroupName $rg -ServerName $SQLsn -DatabaseName $dbn

$time = (Get-Date).ToString()
'Sql Database ' + $dbn + ': Get database object (' + $time + ').'

$db | Add-AzureRmSqlDatabaseToFailoverGroup -ResourceGroupName $rg -ServerName $SQLsn -FailoverGroupName $fogn

$time = (Get-Date).ToString()
'FailoverGroup ' + $fogn + ': add database to failover group (' + $time + ').'