﻿
$global:lc = $null
$global:lc2 = $null
$global:rg = $null
$global:SQLsn = $null
$global:SQLsn2 = $null
$global:kvn = $null
$global:fogn = $null
$global:stan = $null

$global:lc = (Get-AzureRmLocation)[0].Location
$global:lc2 = (Get-AzureRmLocation)[1].Location
$global:rg = 'jfuchsResourceGroup'

$global:SQLsn = (Get-AzureRmSqlServer -ResourceGroupName $global:rg)[0].ServerName
$global:SQLsn2 = (Get-AzureRmSqlServer -ResourceGroupName $global:rg)[1].ServerName
$global:kvn = (Get-AzureRmKeyVault -ResourceGroup $rg)[0].VaultName
$global:fogn = (Get-AzureRmSqlDatabaseFailoverGroup -ResourceGroupName $rg -ServerName $SQLsn).FailoverGroupName
$global:stan = (Get-AzureRmStorageAccount -ResourceGroupName $rg)[0].StorageAccountName

'$lc:: ' + $global:lc
'$lc2:: ' + $global:lc2
'$rg:: ' + $global:rg
'$SQLsn:: ' + $global:SQLsn
'$SQLsn2:: ' + $global:SQLsn2
'$kvn:: ' + $global:kvn
'$fogn:: ' + $global:fogn
'$stan:: ' + $global:stan
