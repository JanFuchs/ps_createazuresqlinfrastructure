﻿#Create Sql Server

$prefix = 'apdcwpoc'
$prefixSqlSrv = 'asq'
$prefixKeyVault = 'vlt'
$prefixStorageAccount = 'sta'
$prefixFailOverGroup = 'fog'

$lc = (Get-AzureRmLocation)[0].Location
$lc2 = (Get-AzureRmLocation)[1].Location

$rg = 'jfuchsResourceGroup'

#Create main SQL Server
$SQLsn = $prefix + $prefixSqlSrv + '001'
$SQLlogin = 'fuchs'
$SQLpw = 'a+201712'
$SQLpws = ConvertTo-SecureString -String $SQLpw -AsPlainText -Force
$cd = New-Object -TypeName System.Management.Automation.PSCredential `
        -ArgumentList $SQLlogin, $SQLpws

New-AzureRmSqlServer -ResourceGroupName $rg -ServerName $SQLsn -Location $lc -SqlAdministratorCredentials $cd

$time = (Get-Date).ToString()
'Sql Server ' + $SQLsn + ' is created (' + $time + ').'

New-AzureRmSqlServerFirewallRule -ResourceGroupName $rg -ServerName $SQLsn -AllowAllAzureIPs

$time = (Get-Date).ToString()
'Sql Server ' + $SQLsn + ': Firewall rule to allow Azure access to SQL Server created (' + $time + ').'

#use current aad user as aad admin on sqlserver
$SQLaadad = (Get-AzureRmContext)[0].Account.Id

Set-AzureRmSqlServerActiveDirectoryAdministrator -ResourceGroupName $rg -ServerName $SQLsn -DisplayName $SQLaadad

$time = (Get-Date).ToString()
'Sql Server ' + $SQLsn + ': Current AAD User as aad admin on Sql Server deployed (' + $time + ').'


#create Storage Account
$stan = $prefix + $prefixStorageAccount + '001'
$skun = 'Standard_LRS'
New-AzureRmStorageAccount -ResourceGroupName $rg -AccountName $stan -Location $lc -SkuName $skun

$time = (Get-Date).ToString()
'Storage Account ' + $SQLsn + ': Storage Account created (' + $time + ').'

Set-AzureRmSqlServerAuditing -State Enabled -ResourceGroupName $rg -ServerName $SQLsn -StorageAccountName $stan 

$time = (Get-Date).ToString()
'Storage Account ' + $SQLsn + ': Sql Server Auditing enabled (' + $time + ').'

$notemail = 'jfuchs.extern@airplus.com'
Set-AzureRmSqlServerThreatDetectionPolicy -ResourceGroupName $rg -ServerName $SQLsn -StorageAccountName $stan -NotificationRecipientsEmails $notemail -EmailAdmins $false

$time = (Get-Date).ToString()
'Storage Account ' + $SQLsn + ': Sql Server Threat Detection enabled (' + $time + ').'

#add key vault / encryption
$kvn = $prefix + $prefixKeyVault + '001'

#get server:
$SqlSrv = Set-AzureRmSqlServer -ResourceGroupName $rg -ServerName $SQLsn -AssignIdentity

New-AzureRmKeyVault -ResourceGroupName $rg -Location $lc -VaultName $kvn

$time = (Get-Date).ToString()
'Key Vault ' + $kvn + ': KeyVault created (' + $time + ').'

Set-AzureRmKeyVaultAccessPolicy -VaultName $kvn -ObjectId $SqlSrv.Identity.PrincipalId `
        -PermissionsToKeys get, wrapKey, unwrapKey

$time = (Get-Date).ToString()
'Key Vault ' + $kvn + ': AccessPolicy for SqlServer created (' + $time + ').'

$prinid = (Get-AzureRmAdUser -UserPrincipalName $SQLaadad).Id.Guid
Set-AzureRmKeyVaultAccessPolicy -VaultName $kvn -ObjectId $prinid `    -PermissionsToKey decrypt,encrypt,unwrapKey,wrapKey,verify,sign,get,list,update,create,import,delete,backup,restore,recover,purge `    -PermissionsToSecrets get, list, set, delete, recover, backup, restore, purge

$time = (Get-Date).ToString()
'Key Vault ' + $kvn + ': AccessPolicy for current user set (' + $time + ').'

$kn = $prefixSqlSrv + '001key0001'
$k = Add-AzureKeyVaultKey -VaultName $kvn -Name $kn -Destination 'Software'

$time = (Get-Date).ToString()
'Key Vault ' + $kvn + ': Create KeyVaultKey (' + $time + ').'

Add-AzureRmSqlServerKeyVaultKey -ResourceGroupName $rg -ServerName $SQLsn -Key $k.Id

$time = (Get-Date).ToString()
'Key Vault ' + $kvn + ': KeyVaultKey assigned to Sql Server (' + $time + ').'

Set-AzureRmSqlServerTransparentDataEncryptionProtector -ResourceGroupName $rg -ServerName $SQLsn -Type AzureKeyVault -KeyId $k.Id -Force

$time = (Get-Date).ToString()
'Key Vault ' + $kvn + ': SetTDEProtector in Sql Server (' + $time + ').'

#Create failover SQL Server
$SQLsn2 = $prefix + $prefixSqlSrv + '002'

New-AzureRmSqlServer -ResourceGroupName $rg -ServerName $SQLsn2 -Location $lc2 -SqlAdministratorCredentials $cd

$time = (Get-Date).ToString()
'Sql Server ' + $SQLsn2 + ' is created (' + $time + ').'

#Create failover group
$fogn = $prefix + $prefixFailOverGroup + "001"

New-AzureRmSqlDatabaseFailoverGroup `
    -ResourceGroupName $rg `
    -ServerName $SQLsn `
    -PartnerServerName $SQLsn2 `
    -FailoverGroupName $fogn `
    -FailoverPolicy "Automatic" `
    -GracePeriodWithDataLossHours 2

$time = (Get-Date).ToString()
'FailoverGroup ' + $fogn + ': FailoverGroup created (' + $time + ').'